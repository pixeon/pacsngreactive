package com.pixeon.pacsngreactive.series;

import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.time.Duration;
import java.util.Objects;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.FluxExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;
import reactor.test.StepVerifier;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
//@WebFluxTest
//@DataJpaTest
public class SeriesTeste {

  @Autowired
  private WebTestClient webClient;

  @Before
  public void setup() {
    this.webClient = webClient
        .mutate()
        .responseTimeout(Duration.ofSeconds(50))
        .build();
  }

  @Test
  public void teste_getSeries() throws Exception {

    webClient.get().uri("/series/1")
        .accept(MediaType.APPLICATION_STREAM_JSON)
        .exchange()
        .expectStatus().isOk();
  }

  @Test
  public void teste_getImgSeries() throws Exception {

  /* webClient
        .get().uri("/series/img/1206")
        .accept(MediaType.TEXT_EVENT_STREAM)
        .exchange()
        .expectStatus().isOk()
       // .expectHeader().contentType(MediaType.TEXT_EVENT_STREAM)
        .expectBody(String.class)
        .returnResult()
        .getResponseBody()
        .
        .hasSize(25);
        .consumeWith(list-> Assert.assertEquals(list.getResponseBody().size(), 25));

*/



   /* WebClient client = WebClient.create("http://localhost:8071");
    Flux<Resource> stringFlux = client.get().uri("/series/img/id1")
        .accept(MediaType.APPLICATION_OCTET_STREAM)
        .retrieve()
        .bodyToFlux(Resource.class).

    stringFlux.subscribe(System.out::println);*/
//
//Thread.sleep(8000);
  }

  public void tste2() {
    FluxExchangeResult<String> result = webClient
        .get().uri("/series/img/205")
        .accept(MediaType.TEXT_EVENT_STREAM)
        .exchange()
        .expectStatus().isOk()
        //.expectHeader().contentType(MediaType.APPLICATION_OCTET_STREAM)
        .returnResult(String.class);

    Flux<String> eventFux = result.getResponseBody();
    StepVerifier.create(eventFux)
        .consumeNextWith(resource -> System.out.println(resource.toString()))
        .expectNextCount(100).verifyComplete();
  }


  @Test
  public void TesteFlux() {
    WebClient client = WebClient.create("http://localhost:8071");
    client.get().uri("/series/img/205").accept(MediaType.TEXT_EVENT_STREAM)
        .exchange()
        .block()
        .bodyToFlux(String.class)
        .as(StepVerifier::create)
        .expectNextCount(7098)
        .verifyComplete();
        //.subscribe(t -> System.out.println(t));
    // Thread.sleep(80000);

  }

  @Test
  public void hystrixStreamWorks() {
    String url = "http://localhost:8071";
    // you have to hit a Hystrix circuit breaker before the stream sends anything

    WebClient client = WebClient.create(url);

    Flux<byte[]> result = client.get().uri("/series/img/1206")
        .accept(MediaType.APPLICATION_OCTET_STREAM)
        .exchange()
        .block()
        .bodyToFlux(byte[].class);


      try (InputStream in = createInputStream(result)) {
        byte[] data = new byte[5];
        int size = 0;
        while ((size = in.read(data)) > 0) {
          System.out.printf("%s", new String(data, 0, size));
        }
      } catch (IOException e) {
        e.printStackTrace();
      }

//    StepVerifier.create(result)
//        .expectNextCount(25)
//        .thenCancel()
//        .verify();
  }

  static InputStream createInputStream(Flux<byte[]> flux) {

    PipedInputStream in = new PipedInputStream();
    flux.subscribeOn(Schedulers.elastic())
        .subscribe(new PipedStreamSubscriber(in));

    return in;
  }
}

class PipedStreamSubscriber extends BaseSubscriber<byte[]> {

  private final Logger logger = LoggerFactory.getLogger(PipedStreamSubscriber.class);

  private final PipedInputStream in;
  private PipedOutputStream out;

  PipedStreamSubscriber(PipedInputStream in) {
    Objects.requireNonNull(in, "The input stream must not be null");
    this.in = in;
  }

  @Override
  protected void hookOnSubscribe(Subscription subscription) {
    //change if you want to control back-pressure
    super.hookOnSubscribe(subscription);
    try {
      this.out = new PipedOutputStream(in);
    } catch (IOException e) {
      //TODO throw a contextual exception here
      throw new RuntimeException(e);
    }
  }

  @Override
  protected void hookOnNext(byte[] payload) {
    try {
      out.write(payload);
    } catch (IOException e) {
      //TODO throw a contextual exception here
      throw new RuntimeException(e);
    }
  }

  @Override
  protected void hookOnComplete() {
    close();
  }

  @Override
  protected void hookOnError(Throwable error) {
    //TODO handle the error or at least log it
    logger.error("Failure processing stream", error);
    close();
  }

  @Override
  protected void hookOnCancel() {
    close();
  }

  private void close() {
    try {
      if (out != null) {
        out.close();
      }
    } catch (IOException e) {
      //probably just ignore this one or simply  log it
    }
  }
}