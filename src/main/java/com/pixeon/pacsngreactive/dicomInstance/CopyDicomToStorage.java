package com.pixeon.pacsngreactive.dicomInstance;

import com.pixeon.pacsngreactive.series.Series;
import com.pixeon.pacsngreactive.series.SeriesStorageRepository;
import com.pixeon.pacsngreactive.series.Seriesstorage;
import com.pixeon.pacsngreactive.storage.Storage;
import java.util.Optional;
import java.util.function.Supplier;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

@Service
public class CopyDicomToStorage {

  @Autowired
  private SeriesStorageRepository seriesStorageRepository;
  @Autowired
  private DicomInstanceRepository dicomInstanceRepository;
  private final Logger logger = LoggerFactory.getLogger(CopyDicomToStorage.class);

  @Transactional
  public Pair<Storage, Dicominstance> copy(final Storage storage,
      final Dicominstance dicominstance) {
    try {
      Series series = dicominstance.getSeriesStorage().getSeries();
      Storage storageOriginal = dicominstance.getSeriesStorage().getStorage();

      Optional<Seriesstorage> seriesstorageOptional = seriesStorageRepository
          .findAllBySeriesAndStorage(series, storage);

      Seriesstorage seriesstorage = seriesstorageOptional
          .orElseGet(this.createNewSeriesstorage(storage, series, 5l));

      Dicominstance newDicomInstance = dicominstance.clone();

      newDicomInstance.setId(null);
      newDicomInstance.setSeriesStorage(seriesstorage);
      newDicomInstance.setReferenceid(dicominstance.getId());
      Dicominstance dicominstanceSaved = dicomInstanceRepository.save(newDicomInstance);

      return Pair.of(storageOriginal, dicominstanceSaved);
    } catch (Exception e) {
      logger.error("error on copy dicom " + dicominstance.getSopinstanceuid(), e);
      throw new RuntimeException(e);
    }
  }

  private Supplier<Seriesstorage> createNewSeriesstorage(Storage storage, Series series,
      Long transfersyntaxId) {
    return () -> {
      Seriesstorage newSeriesstorage = new Seriesstorage();
      newSeriesstorage.setStorage(storage);
      newSeriesstorage.setSeries(series);
      newSeriesstorage.setTransfersyntaxId(transfersyntaxId);
      newSeriesstorage.setEntityversion(0l);
      return seriesStorageRepository.save(newSeriesstorage);
    };
  }

}
