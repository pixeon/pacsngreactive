package com.pixeon.pacsngreactive.dicomInstance;

import com.pixeon.pacsngreactive.series.Seriesstorage;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Async;

public interface DicomInstanceRepository extends JpaRepository<Dicominstance,Long> {

  @Async("TaskDb")
  CompletableFuture<List<Dicominstance>> findAllBySeriesStorage(Seriesstorage seriesstorage);
}
