package com.pixeon.pacsngreactive.dicomInstance;

import com.pixeon.pacsngreactive.series.Seriesstorage;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Dicominstance implements Cloneable {

  @Id
 // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "dicominstance_gen")
 // @SequenceGenerator(name="dicominstance_gen",sequenceName="dicominstance_seq")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private Long entityversion;
  private Long instancenumber;
  private String sopinstanceuid;
  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "seriesstorage_id")
  private Seriesstorage seriesStorage;
  private Long sopclassId;
  @Column(length = Integer.MAX_VALUE)
  private byte[] json;
  private Long schemaversion;
  private String acquisitiondate;
  private Long acquisitionnumber;
  private String acquisitiontime;
  private Long bitsallocated;
  private Long bitsstored;
  private Long columns;
  private String contentdate;
  private String contenttime;
  private String echonumbers;
  private String imagelaterality;
  private String imagetype;
  private Long numberofframes;
  private String patientorientation;
  private String photometricinterpretation;
  private String receivecoilname;
  private Long rows_;
  private Long samplesperpixel;
  private String slicelocation;
  private String viewposition;
  private Long specificcharacterset;
  private Long filelength;
  private String frameindex;
  private Long referenceid;


  public Long getEntityversion() {
    return entityversion;
  }

  public void setEntityversion(Long entityversion) {
    this.entityversion = entityversion;
  }


  public Long getInstancenumber() {
    return instancenumber;
  }

  public void setInstancenumber(Long instancenumber) {
    this.instancenumber = instancenumber;
  }


  public String getSopinstanceuid() {
    return sopinstanceuid;
  }

  public void setSopinstanceuid(String sopinstanceuid) {
    this.sopinstanceuid = sopinstanceuid;
  }


  public Seriesstorage getSeriesStorage() {
    return seriesStorage;
  }

  public void setSeriesStorage(Seriesstorage seriesStorage) {
    this.seriesStorage = seriesStorage;
  }


  public Long getSopclassId() {
    return sopclassId;
  }

  public void setSopclassId(Long sopclassId) {
    this.sopclassId = sopclassId;
  }


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public byte[] getJson() {
    return json;
  }

  public void setJson(byte[] json) {
    this.json = json;
  }


  public Long getSchemaversion() {
    return schemaversion;
  }

  public void setSchemaversion(Long schemaversion) {
    this.schemaversion = schemaversion;
  }


  public String getAcquisitiondate() {
    return acquisitiondate;
  }

  public void setAcquisitiondate(String acquisitiondate) {
    this.acquisitiondate = acquisitiondate;
  }


  public Long getAcquisitionnumber() {
    return acquisitionnumber;
  }

  public void setAcquisitionnumber(Long acquisitionnumber) {
    this.acquisitionnumber = acquisitionnumber;
  }


  public String getAcquisitiontime() {
    return acquisitiontime;
  }

  public void setAcquisitiontime(String acquisitiontime) {
    this.acquisitiontime = acquisitiontime;
  }


  public Long getBitsallocated() {
    return bitsallocated;
  }

  public void setBitsallocated(Long bitsallocated) {
    this.bitsallocated = bitsallocated;
  }


  public Long getBitsstored() {
    return bitsstored;
  }

  public void setBitsstored(Long bitsstored) {
    this.bitsstored = bitsstored;
  }


  public Long getColumns() {
    return columns;
  }

  public void setColumns(Long columns) {
    this.columns = columns;
  }


  public String getContentdate() {
    return contentdate;
  }

  public void setContentdate(String contentdate) {
    this.contentdate = contentdate;
  }


  public String getContenttime() {
    return contenttime;
  }

  public void setContenttime(String contenttime) {
    this.contenttime = contenttime;
  }


  public String getEchonumbers() {
    return echonumbers;
  }

  public void setEchonumbers(String echonumbers) {
    this.echonumbers = echonumbers;
  }


  public String getImagelaterality() {
    return imagelaterality;
  }

  public void setImagelaterality(String imagelaterality) {
    this.imagelaterality = imagelaterality;
  }


  public String getImagetype() {
    return imagetype;
  }

  public void setImagetype(String imagetype) {
    this.imagetype = imagetype;
  }


  public Long getNumberofframes() {
    return numberofframes;
  }

  public void setNumberofframes(Long numberofframes) {
    this.numberofframes = numberofframes;
  }


  public String getPatientorientation() {
    return patientorientation;
  }

  public void setPatientorientation(String patientorientation) {
    this.patientorientation = patientorientation;
  }


  public String getPhotometricinterpretation() {
    return photometricinterpretation;
  }

  public void setPhotometricinterpretation(String photometricinterpretation) {
    this.photometricinterpretation = photometricinterpretation;
  }


  public String getReceivecoilname() {
    return receivecoilname;
  }

  public void setReceivecoilname(String receivecoilname) {
    this.receivecoilname = receivecoilname;
  }


  public Long getRows_() {
    return rows_;
  }

  public void setRows_(Long rows_) {
    this.rows_ = rows_;
  }


  public Long getSamplesperpixel() {
    return samplesperpixel;
  }

  public void setSamplesperpixel(Long samplesperpixel) {
    this.samplesperpixel = samplesperpixel;
  }


  public String getSlicelocation() {
    return slicelocation;
  }

  public void setSlicelocation(String slicelocation) {
    this.slicelocation = slicelocation;
  }


  public String getViewposition() {
    return viewposition;
  }

  public void setViewposition(String viewposition) {
    this.viewposition = viewposition;
  }


  public Long getSpecificcharacterset() {
    return specificcharacterset;
  }

  public void setSpecificcharacterset(Long specificcharacterset) {
    this.specificcharacterset = specificcharacterset;
  }


  public Long getFilelength() {
    return filelength;
  }

  public void setFilelength(Long filelength) {
    this.filelength = filelength;
  }


  public String getFrameindex() {
    return frameindex;
  }

  public void setFrameindex(String frameindex) {
    this.frameindex = frameindex;
  }


  public Long getReferenceid() {
    return (this.referenceid != null) ? this.referenceid
        : this.getId();
  }

  public void setReferenceid(Long referenceid) {
    this.referenceid = referenceid;
  }

  public Dicominstance clone() throws CloneNotSupportedException {
    return (Dicominstance) super.clone();
  }
}
