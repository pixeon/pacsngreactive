package com.pixeon.pacsngreactive.series;

import com.pixeon.pacsngreactive.storage.Storage;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

@Repository
public interface SeriesStorageRepository extends JpaRepository<Seriesstorage, Long> {

  @Async("TaskDb")
  CompletableFuture<List<Seriesstorage>> findAllBySeries(Series series);

  Optional<Seriesstorage> findAllBySeriesAndStorage(Series series, Storage storage);
}
