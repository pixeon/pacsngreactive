package com.pixeon.pacsngreactive.series;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Series {

  @Id
  private Long id;
  private Long entityversion;
  private Long instancecount;
  private String modality;
  private String seriesdescription;
  private String seriesinstanceuid;
  private Long seriesnumber;
  private Long mppsId;
  private Long studyId;
  private Long entitylockId;
  private java.sql.Timestamp seriesdatetime;
  private Long schemaversion;
  private String bodypartexamined;
  private String contrastbolusagent;
  private String frameofreferenceuid;
  private String institutionaldepartmentname;
  private String institutionname;
  private String laterality;
  private String manufacturer;
  private String manufacturermodelname;
  private String operatorsname;
  private String patientposition;
  private String protocolname;
  private String stationname;
  private String viewposition;


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public Long getEntityversion() {
    return entityversion;
  }

  public void setEntityversion(Long entityversion) {
    this.entityversion = entityversion;
  }


  public Long getInstancecount() {
    return instancecount;
  }

  public void setInstancecount(Long instancecount) {
    this.instancecount = instancecount;
  }


  public String getModality() {
    return modality;
  }

  public void setModality(String modality) {
    this.modality = modality;
  }


  public String getSeriesdescription() {
    return seriesdescription;
  }

  public void setSeriesdescription(String seriesdescription) {
    this.seriesdescription = seriesdescription;
  }


  public String getSeriesinstanceuid() {
    return seriesinstanceuid;
  }

  public void setSeriesinstanceuid(String seriesinstanceuid) {
    this.seriesinstanceuid = seriesinstanceuid;
  }


  public Long getSeriesnumber() {
    return seriesnumber;
  }

  public void setSeriesnumber(Long seriesnumber) {
    this.seriesnumber = seriesnumber;
  }


  public Long getMppsId() {
    return mppsId;
  }

  public void setMppsId(Long mppsId) {
    this.mppsId = mppsId;
  }


  public Long getStudyId() {
    return studyId;
  }

  public void setStudyId(Long studyId) {
    this.studyId = studyId;
  }


  public Long getEntitylockId() {
    return entitylockId;
  }

  public void setEntitylockId(Long entitylockId) {
    this.entitylockId = entitylockId;
  }


  public java.sql.Timestamp getSeriesdatetime() {
    return seriesdatetime;
  }

  public void setSeriesdatetime(java.sql.Timestamp seriesdatetime) {
    this.seriesdatetime = seriesdatetime;
  }


  public Long getSchemaversion() {
    return schemaversion;
  }

  public void setSchemaversion(Long schemaversion) {
    this.schemaversion = schemaversion;
  }


  public String getBodypartexamined() {
    return bodypartexamined;
  }

  public void setBodypartexamined(String bodypartexamined) {
    this.bodypartexamined = bodypartexamined;
  }


  public String getContrastbolusagent() {
    return contrastbolusagent;
  }

  public void setContrastbolusagent(String contrastbolusagent) {
    this.contrastbolusagent = contrastbolusagent;
  }


  public String getFrameofreferenceuid() {
    return frameofreferenceuid;
  }

  public void setFrameofreferenceuid(String frameofreferenceuid) {
    this.frameofreferenceuid = frameofreferenceuid;
  }


  public String getInstitutionaldepartmentname() {
    return institutionaldepartmentname;
  }

  public void setInstitutionaldepartmentname(String institutionaldepartmentname) {
    this.institutionaldepartmentname = institutionaldepartmentname;
  }


  public String getInstitutionname() {
    return institutionname;
  }

  public void setInstitutionname(String institutionname) {
    this.institutionname = institutionname;
  }


  public String getLaterality() {
    return laterality;
  }

  public void setLaterality(String laterality) {
    this.laterality = laterality;
  }


  public String getManufacturer() {
    return manufacturer;
  }

  public void setManufacturer(String manufacturer) {
    this.manufacturer = manufacturer;
  }


  public String getManufacturermodelname() {
    return manufacturermodelname;
  }

  public void setManufacturermodelname(String manufacturermodelname) {
    this.manufacturermodelname = manufacturermodelname;
  }


  public String getOperatorsname() {
    return operatorsname;
  }

  public void setOperatorsname(String operatorsname) {
    this.operatorsname = operatorsname;
  }


  public String getPatientposition() {
    return patientposition;
  }

  public void setPatientposition(String patientposition) {
    this.patientposition = patientposition;
  }


  public String getProtocolname() {
    return protocolname;
  }

  public void setProtocolname(String protocolname) {
    this.protocolname = protocolname;
  }


  public String getStationname() {
    return stationname;
  }

  public void setStationname(String stationname) {
    this.stationname = stationname;
  }


  public String getViewposition() {
    return viewposition;
  }

  public void setViewposition(String viewposition) {
    this.viewposition = viewposition;
  }

}
