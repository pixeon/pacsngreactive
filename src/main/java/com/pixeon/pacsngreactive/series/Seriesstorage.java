package com.pixeon.pacsngreactive.series;

import com.pixeon.pacsngreactive.storage.Storage;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Seriesstorage implements Cloneable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "seriesstorage-gen")
  @SequenceGenerator(name="seriesstorage-gen",sequenceName="seriesstorage_seq")
  private Long id;
  private Long entityversion;
  @ManyToOne
  private Series series;
  @ManyToOne
  private Storage storage;
  private Long transfersyntaxId;


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public Long getEntityversion() {
    return entityversion;
  }

  public void setEntityversion(Long entityversion) {
    this.entityversion = entityversion;
  }


  public Series getSeries() {
    return series;
  }

  public void setSeries(Series series) {
    this.series = series;
  }


  public Storage getStorage() {
    return storage;
  }

  public void setStorage(Storage storage) {
    this.storage = storage;
  }


  public Long getTransfersyntaxId() {
    return transfersyntaxId;
  }

  public void setTransfersyntaxId(Long transfersyntaxId) {
    this.transfersyntaxId = transfersyntaxId;
  }

  public Seriesstorage clone() throws CloneNotSupportedException {
    return (Seriesstorage) super.clone();
  }

}
