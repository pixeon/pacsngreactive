package com.pixeon.pacsngreactive.series;

import com.pixeon.pacsngreactive.storage.service.DicomServiceFacade;
import java.time.Duration;
import java.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * SerieController
 */
@RestController
@RequestMapping("/series")
public class SerieController {

  private final Logger logger = LoggerFactory.getLogger(SerieController.class);

  private SeriesRepository repository;
  private DicomServiceFacade facade;

  public SerieController(SeriesRepository repository, DicomServiceFacade facade) {
    this.repository = repository;
    this.facade = facade;
  }

  @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
  public Mono<Series> getSeries(@PathVariable("id") Long id) {
    logger.debug("getSeries on {}", Thread.currentThread().getName());
    return service(id);
  }

  @Async
  public Mono<Series> service(final Long id) {
    logger.debug("service on {}", Thread.currentThread().getName());
    return Mono.fromFuture(repository.findSeriesById(id)).doOnNext(series -> {
      logger.debug("retorno service {}", Thread.currentThread().getName());
    }).onErrorResume(throwable ->
        Mono.error(throwable));
  }

  @GetMapping(value = "/img/{id}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
  public Flux<String> getImages(@PathVariable("id") Long serieId) {
    logger.debug("getImages on {}", Thread.currentThread().getName());
    final Instant start = Instant.now();
    return facade.getImagesService(serieId)
        .map(resource ->
            {
              try {
                logger.debug("map on {}", Thread.currentThread().getName());
                return resource.getFilename();
              } catch (Exception e) {
                return null;
              }
            }
        )
        .doOnComplete(() -> {
          Duration duration = Duration.between(start, Instant.now());
          logger.debug("Time complete: {}m:{}s:{}mm ", duration.toMinutesPart(),
              duration.toSecondsPart(), duration.toMillisPart());
        });
  }


/*
  @GetMapping(value = "/teste", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
  public Flux<Resource> getSeries() {
    logger.debug("getSeries on {}", Thread.currentThread().getName());
    return dicomLocalStorageService.getDicomFile();
  }*/


}