package com.pixeon.pacsngreactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.reactive.config.EnableWebFlux;

@SpringBootApplication
@EnableWebFlux
@EnableJpaRepositories(basePackages = "com.pixeon.pacsngreactive" )
@Configuration
@EnableAsync
public class PacsngreactiveApplication {

  public static void main(String[] args) {
    SpringApplication.run(PacsngreactiveApplication.class, args);
  }

  @Bean(name = "TaskDefault")
  public TaskExecutor getTask() {
    ThreadPoolTaskExecutor poolTaskExecutor = new ThreadPoolTaskExecutor();
    poolTaskExecutor.setThreadNamePrefix("TaskDefault - ");
    poolTaskExecutor.setMaxPoolSize(10);
    poolTaskExecutor.setCorePoolSize(5);
    return poolTaskExecutor;
  }

  @Bean(name = "TaskDb")
  public TaskExecutor getDbTask() {
    ThreadPoolTaskExecutor poolTaskExecutor = new ThreadPoolTaskExecutor();
    poolTaskExecutor.setThreadNamePrefix("TaskDb - ");
    poolTaskExecutor.setMaxPoolSize(5);
    poolTaskExecutor.setCorePoolSize(5);
    return poolTaskExecutor;
  }


  @Bean(name = "TaskIO")
  public TaskExecutor getIOTask() {
    ThreadPoolTaskExecutor poolTaskExecutor = new ThreadPoolTaskExecutor();
    poolTaskExecutor.setThreadNamePrefix("TaskIO - ");
    poolTaskExecutor.setMaxPoolSize(10);
    poolTaskExecutor.setCorePoolSize(5);
    return poolTaskExecutor;
  }
}

