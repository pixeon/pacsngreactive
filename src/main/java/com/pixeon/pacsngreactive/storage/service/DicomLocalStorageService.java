package com.pixeon.pacsngreactive.storage.service;

import com.pixeon.pacsngreactive.dicomInstance.Dicominstance;
import java.util.concurrent.CompletableFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

@Service("FILESYSTEM")
@Scope("prototype")
public class DicomLocalStorageService implements DicomStorageService {

  private final Logger logger = LoggerFactory.getLogger(DicomLocalStorageService.class);
  @Autowired
  @Qualifier(value = "TaskIO")
  private TaskExecutor taskExecutor;

  @Override
  public CompletableFuture<Resource> getDicomFile(Dicominstance dicominstance) {

    return CompletableFuture.supplyAsync(() -> {
          try {
            logger.debug("get {} on {}", dicominstance.getSopinstanceuid(),
                Thread.currentThread().getName());
            logger.debug("Path {}", resolvePath(dicominstance));

            FileSystemResource resource = new FileSystemResource(this.resolvePath(dicominstance));
//            int randomNum = ThreadLocalRandom.current().nextInt(1, 25 + 1);
//            Thread.sleep(500 * randomNum);
            logger.debug("re get {} on {}", dicominstance.getSopinstanceuid(),
                Thread.currentThread().getName());
            return resource;
          } catch (Exception e) {
            return null;
          }
        },
        taskExecutor);
  }

  @Override
  public String getPathByStorageType(Dicominstance dicominstance) {
    return dicominstance.getSeriesStorage().getStorage().getStoragepath().getPath();
  }


}
