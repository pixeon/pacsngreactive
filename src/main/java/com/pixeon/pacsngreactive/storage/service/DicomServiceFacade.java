package com.pixeon.pacsngreactive.storage.service;

import com.pixeon.pacsngreactive.dicomInstance.DicomInstanceRepository;
import com.pixeon.pacsngreactive.dicomInstance.Dicominstance;
import com.pixeon.pacsngreactive.series.SeriesRepository;
import com.pixeon.pacsngreactive.series.SeriesStorageRepository;
import com.pixeon.pacsngreactive.series.Seriesstorage;
import com.pixeon.pacsngreactive.storage.StorageRepository;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.Resource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Service
public class DicomServiceFacade {

  private final Logger logger = LoggerFactory.getLogger(DicomServiceFacade.class);
  private SeriesRepository repository;
  private SeriesStorageRepository seriesStorageRepository;
  private DicomInstanceRepository dicomInstanceRepository;
  private TaskExecutor taskExecutorRep;
  private TaskExecutor taskExecutorDefault;
  private Map<String, DicomStorageService> services;
  private StorageRepository storageRepository;

  public DicomServiceFacade(SeriesRepository repository,
      SeriesStorageRepository seriesStorageRepository,
      DicomInstanceRepository dicomInstanceRepository,
      @Qualifier(value = "TaskDb") TaskExecutor taskExecutorRep,
      @Qualifier(value = "TaskDefault") TaskExecutor taskExecutorDefault,
      Map<String, DicomStorageService> services,
      StorageRepository storageRepository) {
    this.repository = repository;
    this.seriesStorageRepository = seriesStorageRepository;
    this.dicomInstanceRepository = dicomInstanceRepository;
    this.taskExecutorRep = taskExecutorRep;
    this.taskExecutorDefault = taskExecutorDefault;
    this.services = services;
    this.storageRepository = storageRepository;

  }


  public Flux<Resource> getImagesService(final Long serieId) {
    CompletableFuture<List<Dicominstance>> completableFuture = repository
        .findSeriesById(serieId)
        //.handleAsync((series, throwable) -> series) //TODO melhorar o tratamento de erro
        .thenComposeAsync(series -> seriesStorageRepository.findAllBySeries(series),
            taskExecutorRep)
        .thenComposeAsync(this::processStorage)
        .thenComposeAsync(
            seriesstorage -> dicomInstanceRepository
                .findAllBySeriesStorage(seriesstorage),
            taskExecutorRep);

    Flux<Resource> dicominstanceFlux = Mono.fromFuture(completableFuture)
        .publishOn(Schedulers.fromExecutor(taskExecutorDefault))
        .doOnNext(dicom -> logger
            .debug("chegou a lista com {} on {}", dicom.size(), Thread.currentThread().getName()))
        .flatMapMany(this::getDicomImagensFromStorage);

    return dicominstanceFlux;

  }

  private Flux<Resource> getDicomImagensFromStorage(List<Dicominstance> dicomList) {
    logger.debug("inicio do getDicomImagensFromStorage on {}", Thread.currentThread().getName());

   /* Flux<Resource> objectFlux = Flux.fromStream(dicomList.stream())
        .concatMap(dicominstance -> Mono.fromFuture(getService(dicominstance).getDicomFile(dicominstance)))
     .publishOn(Schedulers.fromExecutor(taskExecutorDefault));
    return objectFlux
        .doOnNext(r -> logger.debug("retorno getDicomImagensFromStorage {} on {}", r.getFilename(),
        Thread.currentThread().getName()));*/

    return Flux.<Resource>create(fluxSink -> {
          try {
            List<CompletableFuture<Resource>> futures = dicomList.stream()
                .map(dicominstance -> getService(dicominstance).getDicomFile(dicominstance)
                    .whenCompleteAsync((resource, throwable) ->
                        Optional.of(resource).ifPresent(
                            r -> {
                              fluxSink.next(r);
                              logger.debug("fluxSink.next {} on {}", r.getFilename(),
                                  Thread.currentThread().getName());
                            }
                        ), taskExecutorDefault))
                .collect(Collectors.toList());

            //Complete after all download
            CompletableFuture<Void> allFutures = CompletableFuture
                .allOf(futures.toArray(new CompletableFuture[futures.size()]));
            allFutures.thenAcceptAsync(aVoid -> fluxSink.complete());
            logger.debug("flux create on {}", Thread.currentThread().getName());
          } catch (Exception e) {
            logger.error("error on getDicomImagensFromStorage", e);
            fluxSink.error(e);
          }
        }

    ).doOnNext(r -> logger.debug("retorno getDicomImagensFromStorage {} on {}", r.getFilename(),
        Thread.currentThread().getName()));
  }


  private DicomStorageService getService(Dicominstance dicominstance) {
    String storagetype = dicominstance.getSeriesStorage().getStorage().getStoragetype();
    return services.get(storagetype);
  }

  private CompletableFuture<Seriesstorage> processStorage(List<Seriesstorage> list) {
    Seriesstorage seriesstorage = list.stream()
        .sorted(DicomStorageService::preferenceToDownload)
        .findFirst()
        .orElse(null);

    if (!DicomStorageService.FILESYSTEM.equals(seriesstorage.getStorage().getStoragetype())) {
      return storageRepository.findStorageByStoragetype(DicomStorageService.FILESYSTEM)
          .thenApply(storage -> {
                logger.debug("Criando Seriesstorage local on {}", Thread.currentThread().getName());
                Seriesstorage newSeriesstorage = new Seriesstorage();
                newSeriesstorage.setStorage(storage);
                newSeriesstorage.setSeries(seriesstorage.getSeries());
                newSeriesstorage.setTransfersyntaxId(seriesstorage.getTransfersyntaxId());
                newSeriesstorage.setEntityversion(0l);
                seriesStorageRepository.save(newSeriesstorage);
                return seriesstorage;
              }
          );
    } else {
      return CompletableFuture.completedFuture(seriesstorage);
    }


  }


}
