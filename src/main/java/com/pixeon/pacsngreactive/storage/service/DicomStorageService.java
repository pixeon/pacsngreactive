package com.pixeon.pacsngreactive.storage.service;

import com.pixeon.pacsngreactive.dicomInstance.Dicominstance;
import com.pixeon.pacsngreactive.series.Series;
import com.pixeon.pacsngreactive.series.Seriesstorage;
import com.pixeon.pacsngreactive.storage.Storage;
import java.util.concurrent.CompletableFuture;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.core.io.Resource;

public interface DicomStorageService {

  String FILESYSTEM = "FILESYSTEM";

  CompletableFuture<Resource> getDicomFile(Dicominstance dicominstance);

  default String resolvePath(Dicominstance dicominstance) {
    Storage storage = dicominstance.getSeriesStorage().getStorage();
    return resolvePath(storage,dicominstance);
  }

  default String resolvePath(Storage storage,Dicominstance dicominstance) {
    Seriesstorage seriesStorage = dicominstance.getSeriesStorage();
    Series series = seriesStorage.getSeries();
    String seriesinstanceuid = series.getSeriesinstanceuid();

    String md5Folder = getMd5Folder(seriesinstanceuid);

    String uuid = storage.getUuid();
    String path = String.join("/",
        getPathByStorageType(dicominstance),
        uuid,
        md5Folder,
        seriesinstanceuid,
        "1.2.840.10008.1.2.4.70",
        getFileName(dicominstance));
    return path;
  }

  default String getFileName(Dicominstance dicominstance) {
    return dicominstance.getSopinstanceuid() + "_" + dicominstance.getReferenceid();
  }

  default String getMd5Folder(final String seriesInstanceUID) {
    final String md5 = DigestUtils.md5Hex(seriesInstanceUID);
    return md5.substring(0, 2) + "/" + md5.substring(2, 2 + 2);
  }

  static int preferenceToDownload(Seriesstorage ss1, Seriesstorage ss2) {
    if (ss1.getStorage().getStoragetype().equals(FILESYSTEM)) {
      return -1;
    } else if (ss2.getStorage().getStoragetype().equals(FILESYSTEM)) {
      return -1;
    } else {
      return ss1.getStorage().getStoragepath().getPriority()
          .compareTo(ss2.getStorage().getStoragepath().getPriority());
    }
  }

  String getPathByStorageType(Dicominstance dicominstance);
}
