package com.pixeon.pacsngreactive.storage.service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.StorageOptions;
import com.pixeon.pacsngreactive.dicomInstance.CopyDicomToStorage;
import com.pixeon.pacsngreactive.dicomInstance.Dicominstance;
import com.pixeon.pacsngreactive.storage.Storage;
import com.pixeon.pacsngreactive.storage.StorageRepository;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

@Service("GCS")
@Scope("prototype")
public class DicomGCSStorageService implements DicomStorageService {

  public static final String STORAGE_IMG_PATH = "/home/igor/Pictures/";
  private final Logger logger = LoggerFactory.getLogger(DicomGCSStorageService.class);
  @Autowired
  @Qualifier(value = "TaskIO")
  private TaskExecutor taskIOExecutor;
  @Qualifier(value = "TaskDefault")
  private TaskExecutor taskDefaultExecutor;
  @Value("${google.credentials}")
  private String jsonPath;
  private com.google.cloud.storage.Storage storage;
  @Autowired
  private StorageRepository storageRepository;
  @Autowired
  private CopyDicomToStorage copyDicomToStorage;


  @PostConstruct
  private void setup() throws IOException {

    GoogleCredentials credentials = GoogleCredentials.fromStream(new FileInputStream(jsonPath))
        .createScoped(Arrays.asList("https://www.googleapis.com/auth/cloud-platform"));
    this.storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
  }


  @Override
  public CompletableFuture<Resource> getDicomFile(final Dicominstance dicominstance) {
    return storageRepository.findAllByOrderByStoragepath_priorityDesc()
        .thenApplyAsync(list -> //TODO testar a capacidade do storage antes
            list.stream().findFirst().orElse(null))
        .thenApply(storage -> copyDicomToStorage.copy(storage, dicominstance))
        .thenApplyAsync(this::downloadDicom, taskIOExecutor);

  }


  private Resource downloadDicom(Pair<Storage, Dicominstance> pair) {
    try {
      if (pair == null) {
        return null;
      }
      Storage storage = pair.getFirst();
      Dicominstance dicominstance = pair.getSecond();
      logger.debug("get {} on {}", dicominstance.getSopinstanceuid(),
          Thread.currentThread().getName());
      logger.debug("Path from {}", resolvePath(storage, dicominstance).substring(1));
      logger.debug("Path to {}", getOrCreateDestinationPath(dicominstance));

      Blob blob = this.storage
          .get(BlobId
              .of(storage.getContainer(), resolvePath(storage, dicominstance).substring(1)));

      blob.downloadTo(getOrCreateDestinationPath(dicominstance));

      FileSystemResource resource = new FileSystemResource(
          "/home/igor/Pictures/" + resolvePath(dicominstance).substring(1));

//            int randomNum = ThreadLocalRandom.current().nextInt(1, 25 + 1);
//            Thread.sleep(500 * randomNum);
      logger.debug("re get {} on {}", dicominstance.getSopinstanceuid(),
          Thread.currentThread().getName());
      return resource;
    } catch (Exception e) {
      logger.error("Error on download by GCS", e);
      return null;
    }
  }

  private Path getOrCreateDestinationPath(Dicominstance dicominstance) throws IOException {
    String target = STORAGE_IMG_PATH + resolvePath(dicominstance).substring(1);
    Path path = Paths.get(target.substring(0, target.indexOf(this.getFileName(dicominstance))));

    if (Files.notExists(path)) {
      Files.createDirectories(path);
    }

    return Paths.get(target);
  }

  @Override
  public String getPathByStorageType(Dicominstance dicominstance) {
    return "";
  }

}
