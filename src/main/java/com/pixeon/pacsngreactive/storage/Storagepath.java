package com.pixeon.pacsngreactive.storage;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Storagepath {

  @Id
  private Long id;
  private Long entityversion;
  private String path;
  private Long priority;
  private Long nodeconfigurationId;
  @OneToOne
  @JoinColumn(name = "storage_id")
  private Storage storage;


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public Long getEntityversion() {
    return entityversion;
  }

  public void setEntityversion(Long entityversion) {
    this.entityversion = entityversion;
  }


  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }


  public Long getPriority() {
    return priority;
  }

  public void setPriority(Long priority) {
    this.priority = priority;
  }


  public Long getNodeconfigurationId() {
    return nodeconfigurationId;
  }

  public void setNodeconfigurationId(Long nodeconfigurationId) {
    this.nodeconfigurationId = nodeconfigurationId;
  }


  public Storage getStorage() {
    return storage;
  }

  public void setStorage(Storage storage) {
    this.storage = storage;
  }

}
