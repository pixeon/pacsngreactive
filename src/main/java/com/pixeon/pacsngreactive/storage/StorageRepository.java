package com.pixeon.pacsngreactive.storage;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

@Repository
public interface StorageRepository extends JpaRepository<Storage, Long> {

  @Async("TaskDb")
  CompletableFuture<List<Storage>> findAllByOrderByStoragepath_priorityDesc();

  @Async("TaskDb")
  CompletableFuture<Storage> findStorageByStoragetype(String type);

}
