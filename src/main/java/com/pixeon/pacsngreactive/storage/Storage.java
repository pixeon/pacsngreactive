package com.pixeon.pacsngreactive.storage;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Storage {
  public static final String FILESYSTEM="FILESYSTEM";
  public static final String GCS="GCS";

  @Id
  private Long id;
  private Long entityversion;
  private String accesskey;
  private String container;
  private String description;
  private String storagetype;
  private String uuid;
  private Long maxstoragelimit;
  @OneToOne(mappedBy = "storage")
  private Storagepath storagepath;


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public Long getEntityversion() {
    return entityversion;
  }

  public void setEntityversion(Long entityversion) {
    this.entityversion = entityversion;
  }


  public String getAccesskey() {
    return accesskey;
  }

  public void setAccesskey(String accesskey) {
    this.accesskey = accesskey;
  }


  public String getContainer() {
    return container;
  }

  public void setContainer(String container) {
    this.container = container;
  }


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  public String getStoragetype() {
    return storagetype;
  }

  public void setStoragetype(String storagetype) {
    this.storagetype = storagetype;
  }


  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }


  public Long getMaxstoragelimit() {
    return maxstoragelimit;
  }

  public void setMaxstoragelimit(Long maxstoragelimit) {
    this.maxstoragelimit = maxstoragelimit;
  }

  public Storagepath getStoragepath() {
    return storagepath;
  }

  public void setStoragepath(Storagepath storagepath) {
    this.storagepath = storagepath;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Storage storage = (Storage) o;
    return Objects.equals(id, storage.id) &&
        Objects.equals(uuid, storage.uuid);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, uuid);
  }
}
